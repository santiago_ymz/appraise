import React from 'react';
import { render } from 'react-dom';
import firebase from 'firebase';
import { BrowserRouter as Router } from 'react-router-dom';

// import App from './components/App';
// import registerServiceWorker from './registerServiceWorker';
import './index.css';

// Routes
import AppRoutes from './routes';

firebase.initializeApp({
	apiKey: "AIzaSyCKjWmTkhTY199KkUu6O9Lk8nQMEZCaccc",
	authDomain: "appraise-6d24a.firebaseapp.com",
	databaseURL: "https://appraise-6d24a.firebaseio.com",
	projectId: "appraise-6d24a",
	storageBucket: "appraise-6d24a.appspot.com",
	messagingSenderId: "910382256607"	
});

render(
	<Router>
		<AppRoutes/>
	</Router>,
	document.getElementById('root')
);
