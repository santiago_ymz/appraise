import React from 'react';
import { Route, Switch } from 'react-router-dom';

import App from './components/App.js';
import Budget from './components/Budget.js';
import Projects from './components/Projects.js';
import Cash from './components/Cash.js';
import Diary from './components/Diary.js';
import Dashboard from './components/Dashboard.js';
import Budget_list from './components/Budget_list.js';

const AppRoutes = () =>
	<App>
		<Switch>
			<Route exact path="/budget" component={Budget_list} />
			<Route exact path="/budget/:id" component={Budget} />
			<Route exact path="/projects" component={Projects} />
			<Route exact path="/cash" component={Cash} />
			<Route exact path="/diary" component={Diary} />
			<Route exact path="/" component={Dashboard} />
		</Switch>
	</App>

export default AppRoutes;