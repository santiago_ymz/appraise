import React, { Component } from 'react';
import firebase from 'firebase';
import './global/css/App.css';


class Tasks extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
			task:'',
			time:'',
			cost:'',
			tasks_list:[]
		};
		this.handleAdd = this.handleAdd.bind(this); 
		console.log(props.budget);
	}
	handleAdd(event) {
		this.taskRef.push({
			task:'tarea1',
			time:'1hrs',
			cost:'50000'
		});
		event.preventDefault();
	}
	componentWillMount() {
		this.taskRef = firebase.database().ref("budget").child(this.props.budget).child('tasks');

		this.taskRef.on('child_added', snapshot =>{
			console.log('tasks: ',snapshot.key);
			this.setState({
				tasks_list: this.state.tasks_list.concat(snapshot.val(), snapshot.key)
			})			
			console.log('tasks: ',snapshot.key);
		});		
	}		
  render() {
    return (
    	<div>	
				<fieldset className="o-form__fieldset">
					<legend className="o-form__legend">Tareas del proyecto</legend>
					<div className="o-form__field is-seventh">
						<input className="o-form__input" type="text" placeholder="ingrese un título descriptivo" name="task"/>
						<label className="o-form__label">Tarea</label>
					</div>
					<div className="o-form__field is-eighth">
						<input className="o-form__input" type="text" placeholder="1" name="time"/>
						<label className="o-form__label">Tiempo</label>
					</div>
					<div className="o-form__field is-eighth">
						<input className="o-form__input" type="text" placeholder="$ 00.000" name="cost"/>
						<label className="o-form__label">Costo</label>
					</div>
					<div className="o-form__field o-form__field--actions is-eighth">
						<button className="o-btn o-btn--third" onClick={this.handleAdd}>Add</button>
						<label className="o-form__label"></label>
					</div>
				</fieldset>
				<table className="o-table">
					<thead className="o-table__thead">
						<tr className="o-table__row">
							<th className="o-table__col">Tarea</th>
							<th className="o-table__col">Hrs</th>
							<th className="o-table__col">Costo</th>
							<th className="o-table__col">Acciones</th>
						</tr>
					</thead>
					<tbody className="o-table__body">
					{
						this.state.tasks_list.map((task,key) =>(
							<tr key={key} className="o-table__row">
								<td className="o-table__col">{task.task}</td>
								<td className="o-table__col">{task.time}</td>
								<td className="o-table__col">{task.cost}</td>
								<td className="o-table__col">
									<button className="o-btn o-btn--edit">/</button>
									<button className="o-btn o-btn--delete">X</button>
								</td>
							</tr>
						))	
					}
					</tbody>
				</table>
			</div>			
    );
  }
}

export default Tasks;
