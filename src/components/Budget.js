// Dependencies
import React, { Component } from 'react';
import firebase from 'firebase';
import './global/css/App.css';

//components
import Tasks from './Tasks.js';

class Budget extends Component {
	constructor(props) {
	  super(props);
	
		this.state = {
			client :'',
			contact :'',
			email :'',
			project :'',
			type_project:''
		};
	  this.handleSubmit = this.handleSubmit.bind(this);
	  this.handleChange = this.handleChange.bind(this);
	}
	handleSubmit(event){
		const budgetId = this.props.match.params.id;
		firebase.database().ref('budget/' + budgetId).update({
			id: this.props.match.params.id,
			client: this.state.client,
			contact: this.state.contact,
			email: this.state.email,
			project: this.state.project,
			type_project: this.state.type_project
		});

		console.log('handleSubmit: ', this.state.project, this.state.client);
		event.preventDefault();
	}
	handleChange(event){
		this.setState({
			[event.target.name]:event.target.value
		});

	}
	componentWillMount() {
		console.log(this.props.match.params.id);
		this.budgetRef = firebase.database().ref("budget").child(this.props.match.params.id);
		
		// this.budgetRef.on('value', snapshot =>{
		// 	this.setState({
		// 		client: snapshot.val().client,
		// 		contact: snapshot.val().contact,
		// 		email: snapshot.val().email,
		// 		project: snapshot.val().project
		// 	})
		// });
	}
	componentWillUnmount() {
		this.budgetRef.off();
	}	
  render() {
    return (
			<section className="c-budget">
				<div className="c-budget__top">
					<h1 className="c-budget__title">Nueva cotización</h1>
					<span className="c-budget__folio">{this.props.match.params.id}</span>
				</div>

				<div className="c-budget__body">
					<form onSubmit={this.handleSubmit} className="o-form">
						<fieldset className="o-form__fieldset">
							<legend className="o-form__legend">Datos del cliente</legend>
							<div className="o-form__field is-half">
								<input className="o-form__input" type="text" placeholder="Razón social de la empresa" name="client" value={this.state.client} onChange={this.handleChange} />
								<label className="o-form__label">Nombre Empresa</label>
							</div>
							<div className="o-form__field is-quarter">
								<input className="o-form__input" type="text" placeholder="Nombre de contacto" name="contact" value={this.state.contact} onChange={this.handleChange} />
								<label className="o-form__label">Representante</label>
							</div>
							<div className="o-form__field is-quarter">
								<input className="o-form__input" type="text" placeholder="mail@email.com" name="email" value={this.state.email} onChange={this.handleChange} />
								<label className="o-form__label">Email</label>
							</div>
						</fieldset>
						<fieldset className="o-form__fieldset">
							<legend className="o-form__legend">Datos del proyecto</legend>
							<div className="o-form__field is-third">
								<input className="o-form__input" type="text" placeholder="ingrese un nombre descriptivo" name="project" value={this.state.project} onChange={this.handleChange} />
								<label className="o-form__label">Nombre del proyecto</label>
							</div>
							<div className="o-form__field is-quarter">
								<label className="o-form__label">Tipo de proyecto</label>
								<select className="o-form__select" name="type_project" value={this.state.type_project} onChange={this.handleChange} >
									<option value="null">Selecciona</option>
									<option value="design">Diseño</option>
									<option value="front-end">Front-end</option>
									<option value="hybrid">Diseño y Front-end</option>
									<option value="mobile">mobile</option>
								</select>
							</div>
						</fieldset>
							
						<Tasks budget={this.props.match.params.id}/>					

						{/*<table className="o-table">
							<thead className="o-table__thead">
								<tr className="o-table__row">
									<th className="o-table__col">Tarea</th>
									<th className="o-table__col">Hrs</th>
									<th className="o-table__col">Costo</th>
									<th className="o-table__col">Acciones</th>
								</tr>
							</thead>
							<tbody className="o-table__body">
								<tr className="o-table__row">
									<td className="o-table__col">Tarea número 1</td>
									<td className="o-table__col">5</td>
									<td className="o-table__col">$ 50.000</td>
									<td className="o-table__col">
										<button className="o-btn o-btn--edit">/</button>
										<button className="o-btn o-btn--delete">X</button>
									</td>
								</tr>
							</tbody>
						</table>*/}
						<section className="o-actions">
							<button type="submit" className="o-btn o-btn--secundary u-pull_left">Guardar</button>
							<button className="o-btn o-btn--primary u-pull_right u-ml20">Generar PDF</button>
							<button className="o-btn o-btn--secundary u-pull_right">Eviar por Email</button>
						</section>
					</form>
				</div>
      </section>    	
    );
  }
}

export default Budget;