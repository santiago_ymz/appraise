import React, { Component } from 'react';
import PropTypes from 'prop-types'; 

//components
import Header from './Header.js';

class Content extends Component {
  static PropTypes = {
    body: PropTypes.object.isRequired
  }	
  render() {
  	const { body } = this.props;
    return (
      <div className="o-content">
        <Header/>
        
      	{ body }
        
      </div>
    );
  }
}

export default Content;
