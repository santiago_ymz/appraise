import React, { Component } from 'react';
import firebase from 'firebase';

//components
// import User from './global/User.js';

class User extends Component {
  constructor(){
    super()
    this.state = {
      user: null
    };
    this.handleAuth = this.handleAuth.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
  }
  componentWillMount(){
    firebase.auth().onAuthStateChanged(user => {
      this.setState({user})
    }) 
  }
  handleAuth(){
    const provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider)
      .then( result => console.log(`${result.user.email} ha iniciado sesión`))
      .catch(error => console.log(`Error ${error.code}: ${error.message}`))
  }
  handleLogout(){
    firebase.auth().signOut()
      .then( result => console.log(`${result.user.email} ha salido`))
      .catch(error => console.log(`Error ${error.code}: ${error.message}`))    
  }
  renderLoginButton(){
    // si esta logueado
    if(this.state.user){
      return(
        <div className="c-nav__user">
          <span className="c-nav__profile">
            <img className="c-nav__image" src={this.state.user.photoURL} alt={this.state.user.displayName}/>  
          </span>
          <p className="c-nav__name">
            {this.state.user.displayName}
            <button className="c-nav__close" onClick={this.handleLogout}>Salir</button>
          </p>
          
        </div>
      );
    }else{
      return(
        <button onClick={this.handleAuth}>Login Google</button>
      );
    }
    // no esta logueado
  } 
  render() {
    return (
      <div>
        {this.renderLoginButton()}
      </div>
    );
  }
}

export default User;
