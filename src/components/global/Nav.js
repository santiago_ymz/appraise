import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

//components
import User from './User.js';

class Nav extends Component {
  static propTypes = {
    items:PropTypes.array.isRequired
  };
  render() {
    const { items } = this.props;
    return (
      <nav className="c-nav">
        <User />
        <ul className="c-nav__items">
          {
            items && items.map(
              (item, key) => 
              <li key={key} className="c-nav__item">
                <Link className="c-nav__link" to={ item.url }>
                  <i className="c-nav__icon"></i>
                  <span className="c-nav__text">{ item.title }</span>
                </Link>
              </li>
            )
          }
          </ul>
      </nav>
    );
  }
}

export default Nav;

