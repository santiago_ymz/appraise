import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './global/css/App.css';
// import firebase from 'firebase';

// Data
import items from './global/menu.js';

//components
import Nav from './global/Nav.js';
import Content from './global/Content.js';

class App extends Component {
  static PropTypes = {
    children: PropTypes.object.isRequired
  }
  render() {
    const { children } = this.props;
    return (
      <div className="app">
        <Nav items={ items }/>
        <Content body={ children } />
      </div>
    );
  }
}

export default App;
