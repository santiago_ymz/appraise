import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Link } from 'react-router-dom';
import './global/css/App.css';
import firebase from 'firebase';

class Budget_list extends Component {
	constructor() {
		super()
		this.state = {
			state: 'open',
			budgetId:0,			
			budget_list:[]
		}
		this.handleCreateBudget = this.handleCreateBudget.bind(this);
	}
	handleCreateBudget() {
		this.budgetRef.push({state: this.state.state})
		.then((snap) => {
			const key = snap.key;
			this.setState({
				budgetId: key
			});
			this.setState({
				redirect: true
			});
		});	
	}
	componentWillMount() {  
		this.budgetRef = firebase.database().ref("budget");

		
		this.budgetRef.on('child_added', snapshot =>{
			this.setState({
				budget_list: this.state.budget_list.concat(snapshot.val())
			})
			console.log(this.state.budget_list);
		});
	}	
	componentWillUnmount() {
		this.budgetRef.off();
	}
  render() {
		if (this.state.redirect) {
			return <Redirect push to={`/budget/${this.state.budgetId}`} />;
		}
    return (
    	<section className="c-budget">
				<ul className="c-budget__list">
					{
						this.state.budget_list.map((budget, key) =>(
							<li key={key} className="c-budget__item">
								<p>{budget.state}</p>
								<p>{budget.project}</p>
                <Link className="c-nav__link" to={`/budget/${budget.id}`}>
                  <i className="c-nav__icon"></i>
                  <span className="c-nav__text"> aca {budget.id}</span>
                </Link>								
							</li>
						))
					}	
				</ul>
				<button onClick={this.handleCreateBudget}>Crear Cotización</button>
    	</section>      
    );
  }
}

export default Budget_list;
